var express    = require('express');
var mongoose   = require('mongoose');
var bodyParser = require('body-parser');
var Team       = require('./app/models/teams');

var app = express();
dbUri = process.env.MONGOLAB_URI || 'mongodb://localhost/formulastudent';
mongoose.connect(dbUri);

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;

// ROUTES FOR OUR API
// ==========================================================================================
var router = express.Router();

router.get("/", function (req, res) {
  res.json({ messsage: "Welcome to Formula Student API!" });
});

router.route("/teams")
  .get(function (req, res) {
    Team.find().select('-__v').exec(function (err, teams) {
      if (err)
        res.send(err);

      var jsonRes = {
        "data": {
          "currentItemCount": teams.length,
          "items": teams
        }
      };
      res.json(jsonRes);
    });
  })
  .post(function (req, res) {
    var body = req.body;
    var team = new Team();
    team.name = body.name;
    team.country = body.country;
    team.university = body.university;
    team.carNumber = body.carNumber;
    team.twitter = body.twitter;
    team.facebook = body.facebook;
    team.youtube = body.youtube;

    team.save(function (err, teamPost) {
      if (err)
        res.send(err);

      var jsonRes = {
        "data": {
          "currentItemCount": 1,
          "items": teamPost
        }
      };

      res.json(jsonRes);
    });
  });

router.route("/team/:id")
  .get(function (req, res) {
    var teamId = req.params.id;
    Team.findOne({ _id: teamId }).select("-__v").exec(function (err, team) {
      if (err)
        res.send(err);

      var jsonRes = {
        "data": {
          "currentItemCount": 1,
          "items": team
        }
      };

      res.json(jsonRes);
    });
  })
  .put(function (req, res) {
    
  })
  .delete(function (req, res) {
    var teamId = req.params.id;
    Team.remove({ _id: teamId }, function (err, team) {
      if (err)
        res.send(err);

      res.status(204).end();
    });
  });

// REGISTER OUR ROUTES -------------------------------------------
app.use('/api', router);

// START SERVER
// ==========================================================================================
app.listen(port);
console.log('Magic happens on port ', port);