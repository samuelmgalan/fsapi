var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var TeamSchema = new Schema({
  name: { type: String, required: true },
  country: { type: String, required: true },
  university: { type: String, required: true },
  carNumber: { type: Number, required: true },
  twitter: String,
  facebook: String,
  youtube: String
});

module.exports = mongoose.model('Team', TeamSchema);